﻿using Module1;
using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Entity value = new Entity { a = 10, b = 20 };
            Module1 inst = new Module1();
            Console.WriteLine(inst.SwapItems(value.a, value.b));
            int[] input = { 5, 4, 5, 7, 2, 4, 1, 6, 8, };
            Console.WriteLine(inst.GetMinimumValue(input));
            Console.ReadKey();
        }
        public int[] SwapItems(int a, int b)
        {
            int[] array = { b, a };
            return array;
        }
        public int GetMinimumValue(int[] input)
        {
            int minValue = input[0];
            for (int i = 0; i < input.Length; i++)
            {
                if (minValue > input[i])
                {
                    minValue = input[i];
                }
            }
            return minValue;
        }
    }
}
